#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAX_queue 20

#define PLUS 1000
#define MINUS 1001
#define MUL 1002
#define DIVS 1003
#define NONE 1004

int queue[MAX_queue];
int front_queue = 0;
int rear_queue = -1;
int itemCount_queue = 0;


bool isEmpty_queue() {
   return itemCount_queue == 0;
}

bool isFull_queue() {
   return itemCount_queue == MAX_queue;
}

int size_queue() {
   return itemCount_queue;
}  

void insert_queue(int data) {

   if(!isFull_queue()) {
	
      if(rear_queue == MAX_queue-1) {
         rear_queue = -1;            
      }       

      queue[++rear_queue] = data;
      itemCount_queue++;
   }
}

int removeData_queue() {
	if (isEmpty_queue())
		return(NONE);
		
   int data = queue[front_queue++];
	
   if(front_queue == MAX_queue) {
      front_queue = 0;
   }
	
   itemCount_queue--;
   return data;  
}
