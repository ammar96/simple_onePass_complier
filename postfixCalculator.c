#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAX_queue 20

#define PLUS 1000
#define MINUS 1001
#define MUL 1002
#define DIVS 1003
#define NONE 1004

int stack[20];
int top_stack = -1;
 
void push_stack(int x)
{
        stack[++top_stack] = x;
}
 
int pop_stack()
{
        return stack[top_stack--];
}
 
int calPostfix() {
   		int n1, n2, n3, num, e;
        e = removeData_queue();
        while(e != NONE)
        {
        		if (e == PLUS) {
        			n1 = pop_stack();
                    n2 = pop_stack();
                    n3 = n2 + n1;
                    push_stack(n3);
                    
				}
				else if (e == MINUS) {
        			n1 = pop_stack();
                    n2 = pop_stack();
                    n3 = n2 - n1;
                    push_stack(n3);
                    
				}
				else if (e == MUL) {
        			n1 = pop_stack();
                    n2 = pop_stack();
                    n3 = n2 * n1;
                    push_stack(n3);
                    
				}
				else if (e == DIVS) {
        			n1 = pop_stack();
                    n2 = pop_stack();
                    n3 = n2 / n1;
                    push_stack(n3);
                    
				}
                else
                {
                        push_stack(e);
                }
				e = removeData_queue();
        }
    int retVal = pop_stack();
    printf("%d\n", retVal);
    return retVal;
}  
